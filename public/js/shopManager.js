
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    console.log("User Exists ",user);
  } else {
    // No user is signed in.
    console.log("No User ");
  }
});

function getUserID(){
    var user = firebase.auth().currentUser;
    console.log(user);
    if(user===null){
        window.location.href = "http://localhost:5000/login";
        console.log("NO user");
        return null;
    }
    console.log("User id=",user.uid);
    return user.uid;

}



async function addToShoppingCart(id,name,img_url,price){

    var dbRef = firebase.firestore().collection('Users').doc(getUserID());
    price = parseInt(price);
    var item_data = {
                    id,
                    name,
                    img_url,
                    price,
                    quantity:1
    };

    return dbRef.update({
        cart : firebase.firestore.FieldValue.arrayUnion(item_data)
    })
    .then(function(){
         $.notiny({ text: 'Item '+name+' Added To Cart', image: img_url});
         return;
    })
    .catch(function(err){
         $.notiny({ text: 'Unable to Add Item '+name+' to Cart', image: img_url});
         return;
    })      
 
    
}

function addToShoppingCartAndCheckout(id,name,img_url,price){
    return addToShoppingCart(id,name,img_url,price).then(function(){
        window.location.href = "http://localhost:5000/cart";
        return;
    })
    .catch(function(err){
        console.error(err);
         $.notiny({ text: 'Unable to Add Item '+name+' to Cart', image: img_url});
    })
}



var item_html =`<tr>
                  <th scope="row">
                    <div class="p-2">
                      <img src="{{img_url}}" alt="" width="70" class="img-fluid rounded shadow-sm">
                      <div class="ml-3 d-inline-block align-middle">
                        <h5 class="mb-0"> <a href="#" class="text-dark d-inline-block">{{product_name}}</a></h5><span class="text-muted font-weight-normal font-italic">ID: {{id}}</span>
                      </div>
                    </div>
                    <td class="align-middle"><strong>र{{price}}</strong></td>
                     <td class="border-0 align-middle">
                        <button class="btn">-</button>
                        <strong>{{quanitity}}</strong>
                        <button class="btn">+</button>
                   </td>
                    <td class="align-middle"><a href="#" class="text-dark"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>`;

var cart_html = "";

function getCartHTML(){
    cart_html = "";
    shopping_cart.forEach(function(item){
        var copy_item_html = item_html;
        copy_item_html = copy_item_html.replace("{{img_url}}",item.img_url);
        copy_item_html = copy_item_html.replace("{{product_name}}",item.name);
        copy_item_html = copy_item_html.replace("{{id}}",item.id);
        copy_item_html = copy_item_html.replace("{{price}}",item.price);
        copy_item_html = copy_item_html.replace("{{quanitity}}",item.quantity);
        cart_html = cart_html + copy_item_html;
    })
    $('#content').html(cart_html);
}