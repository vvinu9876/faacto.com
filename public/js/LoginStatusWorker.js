var isLoggedIn = false;

firebase.auth().onAuthStateChanged(function(user){
    if(user){
        isLoggedIn = true;
    }
    else{
        isLoggedIn = false;
    }
})


function isLoggedIn(){
    return isLoggedIn;
}