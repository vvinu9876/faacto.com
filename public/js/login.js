var login_data = {
    email : null,
    password : null,
}
var login_token = null;

var loginFunc = firebase.functions().httpsCallable('login');

async function login(){
    if(login_data.email===null || login_data.email===undefined ||
       login_data.password===null || login_data.password===undefined){
           window.alert("Please Fill in All Details");
           return;
    }
    console.log(login_data);
    return loginFunc({email:login_data.email,password:login_data.password})
           .then(function(result){
                console.log(result);
                if(result.data.status==="failure"){
                    window.alert(result.data.message);
                    return;
                }
                login_token= result.data.token;
                loginWithToken(login_token);
                return;
        })
        .catch(function(err){
            console.error(err);
            window.alert("Something Went Wrong.Please Try Again");
            return;
        })
}

function loginWithToken(token){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    .then(function(){
        return firebase.auth().signInWithCustomToken(token)
                .then(function(){
                        window.location.href = "http://localhost:5000/";
                })
                .catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        console.log(errorMessage);
                        window.alert("Something Went Wrong");
                        // ...
                });
    })
    .catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        console.log(errorMessage);
                        window.alert("Something Went Wrong");
                        // ...
    });
    
}

$('#email').change(function(){
    login_data['email'] = $(this).val();
})

$('#password').change(function(){
    login_data['password'] = $(this).val();
})

$('#submit').click(function(){
    console.log("button clicked");
    login();
})

/*========================

GOOGLE LOGIN

=========================*/
$('#google').click(function(){
    googleSignin();
})

var provider = new firebase.auth.GoogleAuthProvider();

function googleSignin(){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    .then(function(){


    return firebase.auth().signInWithPopup(provider).then(async function(result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        console.log(user);
        await addUserToDb(user);
        window.location.href = "http://localhost:5000/";
        // ...
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;

        console.error(error);
        // ...
    });

    })
    .catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        console.log(errorMessage);
                        window.alert("Something Went Wrong");
                        // ...
    });
}


async function addUserToDb(user){
    var user_data = {
        fname : user.displayName,
        lname : null,
        email : user.email,
        phone : user.phoneNumber,
    }
    console.log("User id =",user.uid);
    return firebase.firestore().collection('Users').doc(user.uid).get().then(function(doc){

        if(doc.exists){
            console.log("Doc Already Exists");
            return;
        }
    
        return firebase.firestore().collection("Users").doc(user.uid).set(user_data).then(function(){
            console.log("User Added To DB Succesfully");
            return;
        })
        .catch(function(err){
            console.error(err);
            console.log("Error Adding user to db ");
            return;
        })

    })
    .catch(function(err){
        console.error(err);
        console.log("Error Adding user to db ");
        return;
    })
}
