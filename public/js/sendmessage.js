function sendmessage(){
    window.alert("Sending Message...");
    var data = {
        name : $('#form_name').val(),
        email : $('#form_email').val(),
        subject : $('#form_subject').val(),
        message : $('#form_message').val(),
        date : new Date()
    }

    return firebase.firestore().collection('Message').add(data).then(function(){
        window.alert("Message sent Succesfully");
    })
    .catch(function(err){
        console.error(err);
        window.alert("Unable to send Message");
    })
}