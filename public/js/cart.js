var cart_data = [];

setLoading();
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    console.log("User Exists ",user);
    getCartData();
  } else {
    // No user is signed in.
    console.log("No User ");
    window.location.href = "http://localhost:5000/login";
  }
});

function getUserID(){
    var user = firebase.auth().currentUser;
    console.log(user);
    if(user===null){
        window.location.href = "http://localhost:5000/login";
        console.log("NO user");
        return null;
    }
    console.log("User id=",user.uid);
    return user.uid;

}

function getCartData(){
    var uid = getUserID();
    var cart = [];

    return firebase.firestore().collection('Users').doc(uid).onSnapshot(function(doc){
        if(!doc.exists){
            window.alert("Something Went Wrong");
            return;
        }
        var data = doc.data().cart;
        cart = data;
        cart_data = data;
        setOrderSummaryHTML(cart);
        if(cart_data.length===0){
            setNoItem();
            return;
        }
        setCartItemHTML(cart);
    },function(err){
        console.error(err);
        window.alert("Something Went Wrong");
        return;
    })
    

}

var item_html =`<tr>
                  <th scope="row">
                    <div class="p-2">
                      <img src="{{img_url}}" alt="" width="70" class="img-fluid rounded shadow-sm">
                      <div class="ml-3 d-inline-block align-middle">
                        <h5 class="mb-0"> <a href="#" class="text-dark d-inline-block">{{product_name}}</a></h5><span class="text-muted font-weight-normal font-italic">ID: {{id}}</span>
                      </div>
                    </div>
                    <td class="align-middle"><strong>र {{price}}</strong></td>
                     <td class="align-middle">
                        <button class="btn" onclick="addQuantity({{id}},'sub')">-</button>
                        <strong>{{quanitity}}</strong>
                        <button class="btn" onclick="addQuantity({{id}},'add')">+</button>
                   </td>
                    <td class="align-middle"><a href="javascript:void(0)" onclick="removeItemFromCart({{id}})" class="text-dark"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>`;

var cart_html = "";

function setLoading(){
    $("#content").html("<h1 class='m-4'>Loading...</h1>");
}

function setNoItem(){
    $("#content").html("<h1 class='m-4'>No Item Added</h1>");
}

function setCartItemHTML(shopping_cart){
    cart_html = "";
    shopping_cart.forEach(function(item){
        var copy_item_html = item_html;
        copy_item_html = copy_item_html.replace("{{img_url}}",item.img_url);
        copy_item_html = copy_item_html.replace("{{product_name}}",item.name);
        copy_item_html = copy_item_html.replace("{{id}}",item.id); //for id
        copy_item_html = copy_item_html.replace("{{id}}",item.id); //for function sub
        copy_item_html = copy_item_html.replace("{{id}}",item.id); //for function add
        copy_item_html = copy_item_html.replace("{{id}}",item.id); //for function remove
        copy_item_html = copy_item_html.replace("{{price}}",item.price);
        copy_item_html = copy_item_html.replace("{{quanitity}}",item.quantity);
        cart_html = cart_html + copy_item_html;
    })
    $('#content').html(cart_html);
}


var orderSummaryList = `
        <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Order summary </div>
          <div class="p-4">
            <p class="font-italic mb-4">Shipping and additional costs are calculated based on values you have entered.</p>
            <ul class="list-unstyled mb-4">
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Order Subtotal </strong><strong>र {subtotal}</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Shipping and handling</strong><strong>र {shipping_charge}</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Tax</strong><strong>र {tax}</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Total</strong>
                <h5 class="font-weight-bold">र {total}</h5>
              </li>
            </ul>
            <button onclick="window.location.href='http://localhost:5000/checkout'" class="btn btn-dark rounded-pill py-2 btn-block">Procceed to checkout</a>
          </div>
        </div>`

function setOrderSummaryHTML(shopping_cart){
    var total = 0;
    shopping_cart.forEach(function(item){
        total = total + (parseInt(item.price) * item.quantity);
    })
    var new_html = orderSummaryList;
    new_html = new_html.replace('{subtotal}',total);
    new_html = new_html.replace('{shipping_charge}',0);
    new_html = new_html.replace('{tax}',0);
    new_html = new_html.replace('{total}',total);

    $('#orderSummary').html(new_html);
}

function getItemIndex(id){
    var index = -1;
    id = id.toString();
    for(var i=0;i<cart_data.length;i++){
        console.log(cart_data[i].id,id);
        if(cart_data[i].id===id){
            index = i;
        }
    }
    return index;
}

function addQuantity(id,type){
    var user_id = getUserID();
    var index = getItemIndex(id);
    if(index===-1){
        console.log("Not found");
        return;
    }
    console.log(cart_data[index]);
    if(type==="add")
        cart_data[index].quantity = cart_data[index].quantity + 1;
    else
        cart_data[index].quantity = cart_data[index].quantity - 1;
    
    if(cart_data[index].quantity<=0){
        removeItemFromCart(id);
        return;
    }

    return firebase
           .firestore()
           .collection('Users')
           .doc(user_id)
           .update({cart:cart_data})
           .then(function(){
                console.log("Updated Cart");
    })
    .catch(function(err){
        console.error(err);
    })
}

function removeItemFromCart(id){
    var user_id = getUserID();
    var index = getItemIndex(id);

    if(index===-1){
        window.alert("something Went Wrong");
        return;
    }

    var removableItem = cart_data[index];
    console.log("Index = ",index);
    console.log(removableItem);
    return firebase.firestore().collection('Users').doc(user_id)    
            .update({
                cart : firebase.firestore.FieldValue.arrayRemove(removableItem)
            })
            .then(function(){
                console.log("succesfully Removed");
            })
            .catch(function(err){
                console.log(err);
            })

}