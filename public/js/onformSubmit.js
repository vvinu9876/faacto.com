

function formSubmit(e){
    e.preventDefault(); //This will prevent the default click action
    console.log("We are working on it");

    window.alert("Requesting Appointment...");

    var data = {
        name : $('#appointment-form').find('input[name="name"]').val(),
        phone : $('#appointment-form').find('input[name="phone"]').val(),
        appointment_date : $('#appointment-form').find('input[name="date"]').val(),
        time : $('#appointment-form').find('input[name="time"]').val(),
        message :  $('#appointment-form').find('input[name="message"]').val(),
        date : new Date(),
    }
    
    console.log("data = ",data);

    return firebase.firestore().collection('AppointmentRequest').add(data).then(function(){
        window.alert("Appointment Requested Succesfully");
    })
    .catch(function(err){
        console.error(err);
        window.alert("Unable to Request Appointment");
    })
}