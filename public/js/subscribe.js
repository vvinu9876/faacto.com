function subscribe(){

    window.alert("Requesting for Subscription...");
    document.body.style.cursor = "wait";
    
    var email_id = $('#subscribe_email_id').val();

    var data = {
        email_id,
        date : new Date(),
    }


    return firebase.firestore().collection('Subscribers').add(data).then(function(){
        window.alert("Succesfully Subscribed");
        document.body.style.cursor = "default";
    })
    .catch(function(err){
        console.error(err);
        window.alert(err.message);
        document.body.style.cursor = "default";
    })

}