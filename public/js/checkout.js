var cart_data = [];

setLoading();
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    console.log("User Exists ",user);
    getCartData();
  } else {
    // No user is signed in.
    console.log("No User ");
    window.location.href = "http://localhost:5000/login";
  }
});

function getUserID(){
    var user = firebase.auth().currentUser;
    console.log(user);
    if(user===null){
        window.location.href = "http://localhost:5000/login";
        console.log("NO user");
        return null;
    }
    console.log("User id=",user.uid);
    return user.uid;

}

function getCartData(){
    var uid = getUserID();
    var cart = [];

    return firebase.firestore().collection('Users').doc(uid).get().then(function(doc){
        if(!doc.exists){
            window.alert("Something Went Wrong");
            return;
        }
        var data = doc.data().cart;
        cart = data;
        cart_data = data;
        if(cart_data.length===0){
            window.location.href = "http://localhost:5000/message?message=No Items in Cart";
            return;
        }
        setOrderHTML(cart);
        $("#user_id").val(uid);
    })
    .catch((err)=>{
        console.error(err);
        window.alert("Something Went Wrong");
        return;
    });
}



function setLoading(){
    var html =`<li class="list-group-item d-flex justify-content-between lh-condensed">
                    <h6>Loading...</h6>
                </li>`;

    $('#content').html(html);
}

var list_html = `<li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">{name}</h6>
                        <small>{id}</small>
                    </div>
                    <span class="text-muted">र {price} </span>
                </li>`

function setOrderHTML(cart_data){
    var item_html = "";
    var total = 0;
    for(var i=0;i<cart_data.length;i++){
        var new_item = list_html;
        new_item = new_item.replace('{name}',cart_data[i].name);
        new_item = new_item.replace('{price}',cart_data[i].price);
        new_item = new_item.replace('{id}',"ID "+cart_data[i].id);
        item_html = item_html + new_item;
        total = total +  (parseInt(cart_data[i].price) * cart_data[i].quantity); 
    }

    var total_html = list_html;
    total_html = total_html.replace('{name}',"Total (INR)");
    total_html = total_html.replace('{price}',total);
    total_html = total_html.replace('{id}','');

    item_html = item_html + total_html;
    $("#number").html(cart_data.length);
    $('#content').html(item_html)
}


function setStates(){
    var arr = [
        "Karnataka",
        "Andra Pradesh",
        "Arunachal Pradesh",
        "Assam",
        "Bihar",
        "Chhattisgarh",
        "Goa",
        "Gujarat",
        "Haryana",
        "Himachal Pradesh",
        "Jammu and Kashmir",
        "Jharkhand",
        "Kerala",
        "Madya Pradesh",
        "Maharashtra",
        "Manipur",
        "Meghalaya",
        "Mizoram",
        "Nagaland",
        "Orissa",
        "Punjab",
        "Rajasthan",
        "Sikkim",
        "Tamil Nadu",
        "Tripura",
        "Uttaranchal",
        "Uttar Pradesh",
        "West Bengal",
    ];
    var item ="<option value={value}>{value}</option>";
    var itemset = "";
    for(var i=0;i<arr.length;i++){
        var new_item = item;
        new_item = new_item.replace("{value}",arr[i]);
        new_item = new_item.replace("{value}",arr[i]);
        itemset = itemset + new_item;
    }
    $("#state").html(itemset);
}

setStates();