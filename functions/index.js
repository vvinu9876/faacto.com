const functions = require('firebase-functions');
const express = require('express');
const engines = require('consolidate');
var hbs = require('handlebars');
const admin = require('firebase-admin');
const cors = require('cors')({origin: true});

const app = express();
app.engine('hbs',engines.handlebars);
app.set('views','./views');
app.set('view engine','hbs')
app.use(cors);

var serviceAccount = require("./serviceAccounts.json");

hbs.registerHelper('times', function(n, block) {
        var accum = '';
        for(var i = 0; i < n; ++i)
          accum += block.fn(i);
        return accum;
});

hbs.registerHelper("math", function(lvalue, operator, rvalue, options) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);
        
    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
});

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://faacto-462d3.firebaseio.com"
});


var db = admin.firestore();

app.get('/', async (req, res) => {
        console.log("I am from index.");
        var db_result = await getHeaderData();
        console.log("Result =",db_result);
        res.render('index',{db_result});
});


async function getFoundersData(){
    var res = [];
    res = await db.collection('Founders').doc('data').get().then(function(doc){
        if(doc.exists){
            return doc.data().data;
        }
        else{
            return [];
        }
    }).catch(function(err){
        console.error(err);
        return [];
    })
    return res;
}



async function getHeaderData(){
    var res = {phone:null,email:null,address:null};
    res = await db.collection('Header').get().then(function(snap){
        var data = {};
        snap.forEach(function(doc){
            data[doc.id] = doc.data();
        })
        return data;
    }).catch(function(err){
        console.error(err);
        return;
    })
    return res;
}

app.get('/contact',async (req,res)=>{
    return db.collection('Header').doc('header').get().then(function(doc){
        if(doc.exists){
            var data = doc.data();
            
            res.render('contact',{data});
        }
        res.status(400);
        return true;
    })
})

app.get('/demo',(req,res)=>{
    res.render('demo',{val:"abc"}); 
})


async function getShopItems(){
    return db.collection('ShopItems').doc('Items').get().then(function(doc){
        if(!doc.exists){
            console.log("ShopItems Data doesnt Exists");
            return [];
        }
        var data = doc.data();
        data.data.forEach(function(item){
                var html = "";
                for(var i=0;i<item.rating;i++){
                    html = html +"<span class='fa fa-star checked mr-1 ml-1'></span>";
                }
                for(i=0;i<(5-item.rating);i++){
                    html = html +"<span class='fa fa-star mr-1 ml-1'></span>";
                }
                item['rating'] = html;
        })
        return data;
    })
    .catch(function(err){
        console.error(err);
        throw new Error(err.message);
    })
}


function getProduct(items,id){
    for(var i=0;i<items.length;i++){
        if(items[i].id.toString()===id){
            return items[i];
        }
    }
    return -1;
}

app.get('/shop',async (req,res)=>{
    var db_result = await getHeaderData();
    var items = await getShopItems();
    console.log(items);
    db_result['items'] = items;
    console.log("Result =",db_result);
    res.render('shop',{db_result});
})

app.get('/product-desc',async (req,res)=>{
    var db_result = await getHeaderData();
    var items = await getShopItems();
    console.log(items);
    var product_id = req.query.id;
    var product = getProduct(items.data,product_id);
    if(product===-1){
        res.redirect('/message?message=Product Not Found');
    }
    db_result['product'] = product;
    console.log(product);
    res.render('product_description',{db_result});
})

function getFounder(founders,id){
    for(var i=0;i<founders.length;i++){
        if(founders[i].id.toString()===id){
            return founders[i];
        }
    }
    return -1;
}

app.get('/aboutfounders',async (req,res)=>{
    var db_result = await getHeaderData();
    var id = req.query.id;
    var founders = await getFoundersData();
    db_result['founder'] = getFounder(founders,id);
    if(db_result.founder===-1){
        res.redirect('/message?message=Founder Not Found');
        return;
    }
    res.render('aboutFounders',{db_result});
})

app.get('/listfounders',async (req,res)=>{
    var db_result = await getHeaderData();
    var founders = await getFoundersData();
    db_result['founders'] = founders;
    res.render('founderslist',{db_result});
})


async function getStory(){
    var res = await db.collection('OurStory').doc('data').get().then(function(doc){
        if(doc.exists){
            return doc.data().story;
        }
        return "";
    })
    .catch(function(err){
        console.error(err);
        return "";
    })
    return res;
}

app.get('/ourstory',async (req,res)=>{
    var db_result = await getHeaderData();
    db_result['story'] = await getStory();
    res.render('ourstory',{db_result});
})

app.get('/ourtech',async (req,res)=>{
    var db_result = await getHeaderData();
    res.render('ourtech',{db_result});
})

app.get('/service-plan-page',async (req,res)=>{
    var db_result = await getHeaderData();
    res.render('service-plan-page',{db_result});
})

app.get('/login',async (req,res)=>{
    return db.collection('Header').doc('header').get().then(function(doc){
        if(doc.exists){
            var data = doc.data();
            res.render('login',{data});
        }
        res.status(400);
        return true;
    })
})

app.get('/signup',async (req,res)=>{
    return db.collection('Header').doc('header').get().then(function(doc){
        if(doc.exists){
            var data = doc.data();
            res.render('signup',{data});
        }
        res.status(400);
        return true;
    })
    
})

app.get('/cart',async (req,res)=>{
    return db.collection('Header').doc('header').get().then(function(doc){
        if(doc.exists){
            var data = doc.data();
            res.render('cart',{data});
        }
        res.status(400);
        return true;
    })  
})


app.get('/checkout',async (req,res)=>{
    return db.collection('Header').doc('header').get().then(function(doc){
        if(doc.exists){
            var data = doc.data();
            res.render('checkout',{data});
        }
        res.status(400);
        return true;
    })
    
})

app.get('/appointment',async (req,res)=>{
    console.log("I am from index.");
    var db_result = await getHeaderData();
    console.log("Result =",db_result);
    res.render('appointment',{db_result});
    
})

app.get('/blog-single',async (req,res)=>{
    
    console.log("I am from index.");
    var db_result = await getHeaderData();
    console.log("Result =",db_result);
    res.render('blog-single',{db_result});
    
})

app.get('/pricing',async (req,res)=>{
    console.log("I am from index.");
    var db_result = await getHeaderData();
    console.log("Result =",db_result);
    res.render('pricing',{db_result});
})

app.get('/message',async (req,res)=>{
     return db.collection('Header').doc('header').get().then(function(doc){
        if(doc.exists){
            var data = doc.data();
            data["message"] = req.query.message;
            res.render('Message',{data});
        }
        res.status(400);
        return true;
    })
})

app.post('/registerUser',async (req,res)=>{
    var data = {
        fname   : req.param('fname'),
        lname   : req.param('lname'),
        email   : req.param('email'),
        phone   : req.param('phone'),
        password: req.param('password'),
    }
    console.log(data);
    var dbRef = db.collection('Users').doc(data.email);
    return dbRef.get().then(function(doc){
        if(doc.exists){
            res.redirect('/message?message=Username Already Exists');
        }

        return dbRef.set(data).then(function(){
            res.redirect("/login");
            return;
        })
        .catch(function(err){
            console.error(err);
            res.redirect("/message?message=Something Went Wrong");
            return;
        })
        
    })
    .catch(function(err){
        console.error(err);
        res.redirect("/message?message=Something Went Wrong");
    })
})

app.post('/submitOrder',async (req,res)=>{
    var data = {
        user_id : req.param("user_id"),
        fname   : req.param('firstname'),
        lname   : req.param('lastname'),
        email   : req.param('email'),
        phone   : req.param('phone'),
        address : req.param('address'),
        address1: req.param('address2'),
        country : req.param('country'),
        state   : req.param('state'),
        zip     : req.param('zip'),
        payment : "COD",
        cart : [],
        date : new Date(),
    }
    console.log(data);
    return db.collection('Users').doc(data.user_id).get().then(function(doc){

        data.cart = doc.data().cart;
        
        return db.collection('Order').add(data).then(function(){
                clearCart(data.user_id);
                res.redirect("/message?message=Order Placed Succesfully");
                return;
        })
        .catch(function(err){
            console.error(err);
            res.redirect("/message?message=Unable to Place Order. Please try again...")
        })
    })
    .catch(function(err){
            console.error(err);
            res.redirect("/message?message=Unable to Place Order. Please try again...")
    })
    
})


exports.app = functions.https.onRequest(app);

function clearCart(user_id){
    return db.collection('Users').doc(user_id).update({cart:[]}).then(function(){
        console.log("Cart Cleared Succesfully");
        return;
    })
    .catch(function(err){
        console.error(err);
    })
}

async function getAuthToken(user_id){
    return admin.auth().createCustomToken(user_id)
                .then(function(customToken) {
                        // Send token back to client
                        return customToken;
            })
            .catch(function(error) {
                console.log('Error creating custom token:', error);
                throw new Error(error.message);
            });
}

exports.login  = functions.https.onCall((data,context)=>{ 
    var ref = db.collection('Users').doc(data.email);
    return ref.get().then(function(doc){
        if(!doc.exists){
            return {status:'failure',message:"Invalid Username"};
        }
        console.log("Password =",doc.data().password," User Password=",data.password);
        
        if(doc.data().password!==data.password){
            console.log("Invalid Password");
            return {status:'failure',message:"Invalid Password"};
        }
        return getAuthToken(data.email).then(function(customToken){
            if(customToken!==null){
                return {status:'success',token:customToken};
            }
            return {status:'failure',message:"Something Went Wrong"};
        })
        .catch(function(err){
            console.error(err);
            return {status:'failure',message:err.message};
        })
    })
    .catch(function(err){
            console.error(err);
            return {status:'failure',message:err.message};
    })
})